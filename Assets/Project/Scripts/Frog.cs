using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour
{
    [Range(1,5)]
    public int humanType = 1;

    private Animator animator;
    public bool isStopped;
    [SerializeField] private int state;
    private float jumpTimerMax = 7f;
    private float jumpTimerMin = 2f;
    private float jumpTimerCur;
    private float jumpTimer = 0f;
    private int jumpCounterMax;
    private int jumpCounter = 0;
    private float hexTimer;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("HumanType", humanType);
        jumpTimerCur = Random.Range(jumpTimerMin, jumpTimerMax);
        jumpCounterMax = Random.Range(1, 5);
        state = 0;
        jumpTimer = 0f;
        jumpCounter = 0;
        isStopped = true;
        hexTimer = 0f;
    }

    private void Update()
    {
        if (!isStopped)
        {
            hexTimer -= Time.deltaTime;
            jumpTimer += Time.deltaTime;
            if (jumpTimer >= jumpTimerCur)
            {
                if (state == 2)
                {
                    FindObjectOfType<AudioManager>().PlayClip("turn");
                    state = 2;
                    ScreenManager sm = FindObjectOfType<ScreenManager>();
                    if (!sm.isBusy)
                    {
                        sm.isBusy = true;
                        sm.Death(humanType);
                    }
                }
                if (state == 1)
                {
                    FindObjectOfType<AudioManager>().PlayClip("turn", 0.5f);
                    animator.SetTrigger("isHuman");
                    state = 2;
                    jumpTimerMax = Mathf.Max(jumpTimerMax - 1f, 2f);
                    jumpTimerCur = jumpTimerMin;
                    jumpTimer = 0f;
                }
                if (state == 0)
                {
                    jumpCounter++;
                    if (jumpCounter >= jumpCounterMax)
                    {
                        FindObjectOfType<AudioManager>().PlayClip("turn", 0.5f);
                        animator.SetTrigger("isTurning");
                        state = 1;
                        jumpTimerMax = Mathf.Max(jumpTimerMax - 1f, 2f);
                        jumpTimerCur = jumpTimerMax;
                        jumpTimer = 0f;
                    }
                    else
                    {
                        FindObjectOfType<AudioManager>().PlayClip("jump", 0.5f);
                        animator.SetTrigger("isJumping");
                        jumpTimerCur = Random.Range(jumpTimerMin, jumpTimerMax);
                        jumpTimer = 0f;
                    }
                }
            }
        }
    }

    public void HexFrog()
    {
        if (hexTimer > 0) return;
        if (state == 0)
        {
            animator.SetTrigger("isDead");
            ScreenManager sm = FindObjectOfType<ScreenManager>();
            if (!sm.isBusy)
            {
                sm.isBusy = true;
                sm.Death(6);
            }
            hexTimer = 0.5f;
        }
        else if (state == 1)
        {
            animator.SetTrigger("isHexed");
            jumpTimerCur = Random.Range(jumpTimerMin, jumpTimerMax);
            jumpCounterMax = Random.Range(1, 5);
            state = 0;
            jumpTimer = 0f;
            jumpCounter = 0;
            isStopped = false;
            hexTimer = 0.5f;
        }
        else if (state == 2)
        {
            animator.SetTrigger("isHexed");
            jumpTimerCur = Random.Range(jumpTimerMin, jumpTimerMax);
            jumpCounterMax = Random.Range(1, 5);
            state = 1;
            jumpTimer = 0f;
            jumpCounter = 0;
            isStopped = false;
            hexTimer = 0.5f;
        }
    }

    public void ResetFrog()
    {
        animator.SetTrigger("Reset");
        jumpTimerMax = 7f;
        jumpTimerCur = Random.Range(jumpTimerMin, jumpTimerMax);
        jumpCounterMax = Random.Range(1, 5);
        state = 0;
        jumpTimer = 0f;
        jumpCounter = 0;
        isStopped = false;
    }
}
