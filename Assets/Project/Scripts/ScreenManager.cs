using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;

public class ScreenManager : MonoBehaviour
{
    public List<GameObject> Humans;
    public List<GameObject> Frogs;
    public GameObject GameName;
    public GameObject PlayButton;
    public GameObject RetryButton;
    public GameObject HowtoButton;
    public GameObject HowtoButton2;
    public GameObject HowtoText;

    public Animator fadeAnimator;
    public Camera MainCamera;

    public GameObject WitchStart;
    public GameObject WitchEnd;

    public bool isBusy = false;

    public GameObject HexPrefab;
    public AudioMixer audioMixer;

    public UITimer Timer;
    public TextMeshProUGUI BestTime;

    // Start is called before the first frame update
    void Start()
    {
        MainCamera.transform.position = new Vector3(-6, 0, -10);
        GameName.SetActive(true);
        WitchStart.SetActive(true);
        WitchEnd.SetActive(false);
        PlayButton.SetActive(true);
        RetryButton.SetActive(false);
        HowtoButton.SetActive(true);
        HowtoButton2.SetActive(false);
        HowtoText.SetActive(false);
        Timer.ResetTimer();
        Timer.gameObject.SetActive(false);
        BestTime.text = "Best Time " + PlayerPrefs.GetString("BestTime", "-");
        foreach(GameObject human in Humans)
        {
            human.SetActive(false);
        }
        foreach (GameObject frog in Frogs)
        {
            frog.GetComponent<Frog>().isStopped = true;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(LoadMainMenu());
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            float curVolume;
            audioMixer.GetFloat("masterVolume", out curVolume);
            audioMixer.SetFloat("masterVolume", curVolume > -80f ? -80f : 20f);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Instantiate(HexPrefab, mousePos, Quaternion.identity);
            foreach(GameObject frog in Frogs)
            {
                FindObjectOfType<AudioManager>().PlayClip("hex", 0.8f);
                if (Vector2.Distance(new Vector2(frog.transform.position.x, frog.transform.position.y-0.5f), mousePos) <= 0.9)
                {
                    frog.GetComponent<Frog>().HexFrog();
                }
            }
        }
    }

    IEnumerator LoadMainMenu()
    {
        fadeAnimator.SetTrigger("StartTransition");
        yield return new WaitForSeconds(1);
        MainCamera.transform.position = new Vector3(-6, 0, -10);
        GameName.SetActive(true);
        WitchStart.SetActive(true);
        WitchEnd.SetActive(false);
        PlayButton.SetActive(true);
        RetryButton.SetActive(false);
        HowtoButton.SetActive(true);
        HowtoButton2.SetActive(false);
        HowtoText.SetActive(false);
        Timer.ResetTimer();
        Timer.gameObject.SetActive(false);
        BestTime.text = "Best Time " + PlayerPrefs.GetString("BestTime", "-");
        foreach (GameObject human in Humans)
        {
            human.SetActive(false);
        }
        foreach (GameObject frog in Frogs)
        {
            frog.GetComponent<Frog>().isStopped = true;
        }
    }

    public void PlayGame()
    {
        StartCoroutine(PlayGameInner());
    }

    private IEnumerator PlayGameInner()
    {
        fadeAnimator.SetTrigger("StartTransition");
        yield return new WaitForSeconds(1);
        MainCamera.transform.position = new Vector3(5, 0, -10);
        GameName.SetActive(false);
        WitchStart.SetActive(false);
        WitchEnd.SetActive(false);
        PlayButton.SetActive(false);
        RetryButton.SetActive(false);
        HowtoButton.SetActive(false);
        HowtoButton2.SetActive(false);
        HowtoText.SetActive(false);
        Timer.StartTimer();
        Timer.gameObject.SetActive(true);
        BestTime.text = "Best Time " + PlayerPrefs.GetString("BestTime", "-");
        foreach (GameObject human in Humans)
        {
            human.SetActive(false);
        }
        foreach (GameObject frog in Frogs)
        {
            frog.GetComponent<Frog>().ResetFrog();
        }
    }

    public void Death(int type)
    {
        StartCoroutine(DeathInner(type));
    }

    IEnumerator DeathInner(int type)
    {
        FindObjectOfType<AudioManager>().PlayClip("death");
        yield return new WaitForSeconds(0.5f);
        fadeAnimator.SetTrigger("StartTransition");
        yield return new WaitForSeconds(1);
        MainCamera.transform.position = new Vector3(-6, 0, -10);
        GameName.SetActive(false);
        WitchStart.SetActive(false);
        WitchEnd.SetActive(true);
        PlayButton.SetActive(false);
        RetryButton.SetActive(true);
        HowtoButton.SetActive(true);
        HowtoButton2.SetActive(false);
        HowtoText.SetActive(false);
        Timer.StopTimer();
        Timer.gameObject.SetActive(true);
        BestTime.text = "Best Time " + PlayerPrefs.GetString("BestTime", "-");
        foreach (GameObject human in Humans)
        {
            human.SetActive(false);
        }
        foreach (GameObject frog in Frogs)
        {
            frog.GetComponent<Frog>().isStopped = true;
        }
        Humans[type - 1].SetActive(true);
        isBusy = false;
    }
}
