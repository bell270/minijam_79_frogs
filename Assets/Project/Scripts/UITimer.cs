using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour
{
	public TextMeshProUGUI TimerText;
	public bool playing = false;
	private float Timer;

	void Update()
	{
		if (playing == true)
		{

			Timer += Time.deltaTime;
			int minutes = Mathf.FloorToInt(Timer / 60F);
			int seconds = Mathf.FloorToInt(Timer % 60F);
			int milliseconds = Mathf.FloorToInt((Timer * 100F) % 100F);
			TimerText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + milliseconds.ToString("00");
		}
	}

	public void StartTimer()
    {
		Timer = 0f;
		playing = true;
		TimerText.color = new Color(1f, 1f, 1f);
	}

	public void StopTimer()
    {
		playing = false;
		TimerText.color = new Color((float)172 / 255, (float)50 / 255, (float)50 / 255);

		if (PlayerPrefs.GetFloat("BestTimer", 0) < Timer)
        {
			int minutes = Mathf.FloorToInt(Timer / 60F);
			int seconds = Mathf.FloorToInt(Timer % 60F);
			int milliseconds = Mathf.FloorToInt((Timer * 100F) % 100F);
			PlayerPrefs.SetString("BestTime", minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + milliseconds.ToString("00"));
			PlayerPrefs.SetFloat("BestTimer", Timer);
		}
	}

	public void ResetTimer()
	{
		Timer = 0f;
		playing = false;
		TimerText.color = new Color(1f, 1f, 1f);
		int minutes = Mathf.FloorToInt(Timer / 60F);
		int seconds = Mathf.FloorToInt(Timer % 60F);
		int milliseconds = Mathf.FloorToInt((Timer * 100F) % 100F);
		TimerText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + milliseconds.ToString("00");
	}
}
